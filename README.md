# module feduration

## 介绍
webpack5 - module feduration(模块联邦) - 微应用项目开发

相关知识点地址：[前端性能分析](https://gitee.com/max-lu/max-webpack/blob/master/课程6-前端性能分析.md)

基于 js + html

## 目录

mf-light：微应用

mf-container：容器应用

+++

[模块联邦](https://webpack.docschina.org/concepts/module-federation/)

+ webpack5的Module Federation（模块联邦）

+ 实现跨应用 模块共享

  1.容器应用

  2.微应用

```
1.加入ABC三个项目，他们的轮播图都是一样的
2.可以把轮播图抽离出来，打包成一个微应用并导出。供三个项目使用
3.技术栈最好统一：Vue、React、js + html
4.容器应用：使用到微应用的项目，叫容器应用
```

webpack-dev-server的两种方式：

```
"dev": "webpack serve",
"dev1": "webpack-dev-server"
```

## 其他

```
问：容器应用和微应用直接可以通信吗？
答：不可以。就是说通过外部传参改变微应用的功能，目前无法实现
```











