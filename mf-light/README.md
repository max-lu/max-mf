# 微应用

## 配置说明

```
plugins: [
    new ModuleFederationPlugin({
      name: "demoA", // 微应用的别名，一定要唯一，当被其他容器应用使用时，是作为模块名称
      filename: "remoteEntry.js", // 应用打包出来后具体的文件名。导出的文件名称，被其他容器应用引用时的文件名称
      // remotes: {}, // 在容器应用中使用，配置如何使用微应用。因为当前这个是微应用，所以这里用不到，注释掉
      exposes: { // 对外暴露出的内容.如果没有对外暴露，容器是拿不到的
        "./index": "./src/index.js", // 模块名称（./index）：模块路径(./src/index.js); 模块名称必须是相对路径
      },
      // shared: [ // 共享的.比如index.js依赖了react等，可以在这里共享。容器应用在加载微应用的时候会被共享，容器应用就不需要再安装这些依赖了
      //   "react",
      //   "react-dom",
      // ],
    }),
  ]
```

